package scripts.fluffeesCrafter;

import org.tribot.api2007.Skills;
import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.Painting;
import org.tribot.script.interfaces.Starting;
import scripts.banking.Banking;
import scripts.fluffeesapi.data.interactables.banking.InteractableBankEquipment;
import scripts.fluffeesapi.data.interactables.banking.InteractableBankItem;
import scripts.fluffeesapi.data.structures.bag.BagSingleton;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.Mission;
import scripts.fluffeesapi.scripting.frameworks.mission.scriptTypes.MissionManagerScript;
import scripts.fluffeesapi.scripting.painting.scriptPaint.ScriptPaint;
import scripts.fluffeesapi.scripting.swingcomponents.gui.AbstractWizardGui;
import scripts.furnacecrafting.FurnaceCrafting;
import scripts.furnacecrafting.data.Furnaces;
import scripts.furnacecrafting.data.items.Rings;

import java.util.LinkedList;

@ScriptManifest(
        authors     = "Fluffee",
        category    = "Crafting",
        name        = "Fluffees Crafter",
        description = "Crafts items",
        gameMode = 1)

public class Main extends MissionManagerScript implements Starting, Painting {

    private ScriptPaint scriptPaint = new ScriptPaint.Builder(
            ScriptPaint.hex2Rgb("#a1e8af"), "Crafter")
            .addField("Version", Double.toString(1.00))
            .addSkill(Skills.SKILLS.CRAFTING)
            .build();

    private InteractableBankItem[] itemsToWithdraw = new InteractableBankItem[]{
            new InteractableBankItem.Builder("Gold bar", 13).build(),
            new InteractableBankItem.Builder("Sapphire", 13).build(),
            new InteractableBankItem.Builder("Ring mould", 1).build()};
    private InteractableBankItem[] itemsToDeposit = new InteractableBankItem[]{
            new InteractableBankItem.Builder("Sapphire ring", 13).build()};

    private LinkedList<Mission> missionList = new LinkedList<>();

    @Override
    public AbstractWizardGui getGUI() {
        return null;
    }

    @Override
    public ScriptPaint getScriptPaint() {
        return scriptPaint;
    }

    @Override
    public LinkedList<Mission> getMissions() {
        return missionList;
    }

    @Override
    public void initializeMissionList() {
        missionList.add(new FurnaceCrafting(Rings.SAPPHIRE_RING, Furnaces.EDGEVILLE_FURNACE));
        missionList.add(new Banking(itemsToWithdraw, new InteractableBankEquipment[0], itemsToDeposit));
    }

    @Override
    public boolean shouldLoopMissions() {
        return true;
    }

    @Override
    public boolean isMissionValid() {
        return true;
    }

    @Override
    public boolean isMissionCompleted() {
        return getMissions().stream().allMatch((mission) -> mission.isMissionCompleted());
    }

    @Override
    public void onStart() {
        BagSingleton.getInstance().addOrUpdate("shouldBank", true);
    }
}
